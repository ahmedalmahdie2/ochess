﻿/**
************** HEROSFXHANDLER **************
Author: Ahmed Almahdie
Date: 07 - 20 - 2019
Discription:
HeroSFXHandler will handle the sound effects that an individual Hero would play at all times */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NHA.OChess
{
    public class HeroSFXHandler : MonoBehaviour
    {
        #region properties, variables, and declerations

        #endregion

        #region MonoBehaviour methods
        // Start is called before the first frame update
        void Start()
        {
            
        }
    
        // Update is called once per frame
        void Update()
        {
            
        }
        #endregion
    
        #region Helper methods

        #endregion

        #region public methods

        #endregion
    }
}
