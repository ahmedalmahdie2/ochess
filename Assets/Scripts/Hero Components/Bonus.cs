﻿/**
************** Bonus **************
Author: Ahmed Almahdie
Date: 07 - 30 - 2019
Discription:
 Just a holder class for the BonusScriptable object */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NHA.OChess
{
    public enum BonusType
    {
        Shield,
        Health,
        Damage,
        AttackRange,
        MovementRange,
        SpecialAbilityDamage
    }
    
    public class Bonus
    {
        #region properties, variables, and declerations
        public  BonusScriptable Data;
        #endregion
    }
}
