﻿/**
************** ATTACKER **************
Author: Ahmed Almahdie
Date: 07 - 20 - 2019
Discription:
This is an attacker component that will be responsible for general attacking functions */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NHA.OChess
{
    public class Attacker : MonoBehaviour
    {
        #region properties, variables, and declerations
        [SerializeField] protected int _defaultDamage;
        public int DefaultDamage 
        {
            get
            {
                return _defaultDamage;
            }
            set
            {
                _defaultDamage = value;
            }
        }

        private uint _damage;
        public uint Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }

        private Bonus _myBonus;
        public Bonus MyBonus
        {
            get { return _myBonus; }
            set { _myBonus = value; }
        }

        protected Hero myHero;

        #endregion

        #region MonoBehaviour methods
        // Start is called before the first frame update
        protected virtual void Start()
        {
            myHero = GetComponent<Hero>();
        }
    
        // Update is called once per frame
        protected virtual  void Update()
        {
            
        }
        #endregion
    
        #region Helper methods
        protected virtual void attack(Hero attackedEntity, int damage = 1)
        {
            
        }

         protected virtual void playRecieveBonusEffects()
        {
            // some effects
        }

        protected virtual void addBonus(BonusScriptable bonus)
        {
            _myBonus.Data = bonus;
            _damage += _myBonus.Data.UintValue;
        }

        #endregion

        #region public methods
        public virtual void RecieveBonus(BonusScriptable bonus)
        {
            addBonus(bonus);
            playRecieveBonusEffects();
        }
        
        public virtual void Attack(Hero attackedEntity, int damage = 1)
        {
            if(!attackedEntity)
            {
                DebugHelper.DebugLogWithTimeStamp("Got no Attacked Entity", name, GetType());
                return;
            }
        }
        #endregion
    }
}
