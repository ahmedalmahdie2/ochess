﻿/**
************** SHIELD **************
Author: Ahmed Almahdie
Date: 07 - 20 - 2019
Discription:
Shield will be responsible for handling the shield points a hero has */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NHA.OChess
{
    public class Shield : MonoBehaviour
    {
        #region properties, variables, and declerations

        protected uint _sheildValue;
        public uint SheildValue
        {
            get { return _sheildValue; }
            set { _sheildValue = value; }
        }


        protected Bonus _myBonus;
        public Bonus MyBonus
        {
            get { return _myBonus; }
            set { _myBonus = value; }
        }

        #endregion

        #region MonoBehaviour methods
        // Start is called before the first frame update
        void Start()
        {
            
        }
    
        // Update is called once per frame
        void Update()
        {
            
        }
        #endregion
    
        #region Helper methods
        protected virtual void playRecieveBonusEffects()
        {
            // some effects
        }

        protected virtual void addBonus(BonusScriptable bonus)
        {
            _myBonus.Data = bonus;
            _sheildValue += _myBonus.Data.UintValue;
        }

        #endregion

        #region public methods
        public virtual void RecieveBonus(BonusScriptable bonus)
        {
            addBonus(bonus);
            playRecieveBonusEffects();
        }
        #endregion
    }
}
