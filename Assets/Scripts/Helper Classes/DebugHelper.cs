﻿/**
************** DebugHelper **************
Author: Ahmed Almahdie
Date: 07 - 20 - 2019
Discription:
some static functions every class could use */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NHA.OChess
{
    public class DebugHelper
    {
        #region properties, variables, and declerations

        #endregion
    
        #region Helper methods

        #endregion

        #region public methods

        public static void DebugLogWithTimeStamp(string msg, string gameObjectName, Type type = null)
        {
            if(type != null)
            {
                Debug.Log(gameObjectName + "/"+ type.ToString() + " :  "+ msg + ", TIME: " + Time.timeSinceLevelLoad);
            }
            else
            {
                Debug.Log(msg + ", TIME: " + Time.timeSinceLevelLoad);
            }
        }
        #endregion
    }
}
