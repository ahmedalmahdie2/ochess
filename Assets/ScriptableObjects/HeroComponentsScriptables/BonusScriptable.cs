﻿/**
************** BONUS **************
Author: Ahmed Almahdie
Date: 07 - 20 - 2019
Discription:
Bonus will handle bonus abilities for a hero, be it of wshield or hea;th points */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NHA.OChess
{   
    [CreateAssetMenu(fileName = "BonusScriptable", menuName = "OChess/Single Instances/BonusScriptable", order = 0)]
    public class BonusScriptable : ScriptableObject 
    {
        #region properties, variables, and declerations
        public BonusType MyBonusType;
        public float FloatValue;
        public int IntValue;
        public uint UintValue;
        public bool IsConsumable;
        #endregion
    }
}
